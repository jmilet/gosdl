package main

import (
	"fmt"
	"os"

	"jmi.com/graphics/sprite"

	"github.com/veandco/go-sdl2/sdl"
)

const (
	screenWidth = 800
	screenHeigh = 600
)

func main() {
	if err := run(); err != nil {
		fmt.Fprintf(os.Stderr, "%v", err)
		os.Exit(2)
	}
}

func run() error {
	var sprites []*sprite.Sprite

	peepArray := make([]sdl.Event, 3, 3)

	err := sdl.Init(sdl.INIT_EVERYTHING)
	if err != nil {
		return fmt.Errorf("error init %v", err)
	}
	defer sdl.Quit()

	// Window and renderer.
	window, renderer, err := sdl.CreateWindowAndRenderer(screenWidth, screenHeigh, sdl.WINDOW_SHOWN)
	if err != nil {
		return fmt.Errorf("no window %v", err)
	}
	defer window.Destroy()

	running := true
	for running {
		renderer.Clear()

		sdl.PumpEvents()
		numEventsRetrieved, err := sdl.PeepEvents(peepArray, sdl.PEEKEVENT, sdl.FIRSTEVENT, sdl.LASTEVENT)
		if err != nil {
			return fmt.Errorf("PeepEvents error: %v", err)
		}

		for i := 0; i < numEventsRetrieved; i++ {
			fmt.Printf("Event Peeked Value: %v\n", peepArray[i]) // primitive printing of event
		}

		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch t := event.(type) {
			case *sdl.QuitEvent:
				running = false
			case *sdl.MouseMotionEvent:
				fmt.Printf("[%d ms] MouseMotion\ttype:%d\tid:%d\tx:%d\ty:%d\txrel:%d\tyrel:%d\n", t.Timestamp, t.Type, t.Which, t.X, t.Y, t.XRel, t.YRel)
			case *sdl.MouseButtonEvent:
				fmt.Printf("[%d ms] MouseButton\ttype:%d\tid:%d\tx:%d\ty:%d\tbutton:%d\tstate:%d\n", t.Timestamp, t.Type, t.Which, t.X, t.Y, t.Button, t.State)

				if t.State == 0 {
					// Sprite.
					explosion, err := sprite.New("../res/explosion_transparent.png", t.X-32, t.Y-32, 23, 5, 5, 64, 64, renderer)
					if err != nil {
						return fmt.Errorf("no sprite %v", err)
					}
					sprites = append(sprites, explosion)
				}

			case *sdl.MouseWheelEvent:
				fmt.Printf("[%d ms] MouseWheel\ttype:%d\tid:%d\tx:%d\ty:%d\n", t.Timestamp, t.Type, t.Which, t.X, t.Y)
			case *sdl.KeyUpEvent:
				fmt.Printf("[%d ms] Keyboard\ttype:%d\tsym:%c\tmodifiers:%d\tstate:%d\trepeat:%d\n", t.Timestamp, t.Type, t.Keysym.Sym, t.Keysym.Mod, t.State, t.Repeat)
				running = false
			}
		}

		updateSrites(sprites)
		sprites = removeCompleted(sprites)
		drawSrites(sprites, renderer)

		renderer.Present()
		sdl.Delay(1000 / 30)
	}

	destroySprites(sprites)

	return nil
}

func destroySprites(sprites []*sprite.Sprite) {
	for _, spr := range sprites {
		spr.Destroy()
	}
}

func removeCompleted(sprites []*sprite.Sprite) []*sprite.Sprite {
	var res []*sprite.Sprite

	for _, spr := range sprites {
		if spr.Completed {
			spr.Destroy()
		} else {
			res = append(res, spr)
		}
	}

	return res
}

func updateSrites(sprites []*sprite.Sprite) {
	for _, spr := range sprites {
		spr.Update()
	}
}

func drawSrites(sprites []*sprite.Sprite, renderer *sdl.Renderer) {
	for _, spr := range sprites {
		spr.Draw(renderer)
	}
}
