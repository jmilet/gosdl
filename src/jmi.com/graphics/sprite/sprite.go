package sprite

import (
	"fmt"

	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/sdl_image"
)

// Sprite object.
type Sprite struct {
	X            int32
	Y            int32
	currentFrame int
	numFrames    int
	frameColums  int
	frameRows    int
	width        int
	heigh        int
	Completed    bool
	texture      *sdl.Texture
}

// New creates an sprite object.
func New(path string, x, y int32, numFrames, frameColums, frameRows, width, heigh int, renderer *sdl.Renderer) (*Sprite, error) {
	var err error

	sprite := Sprite{}

	sprite.X = x
	sprite.Y = y
	sprite.currentFrame = 0
	sprite.numFrames = numFrames
	sprite.frameColums = frameColums
	sprite.frameRows = frameRows
	sprite.width = width
	sprite.heigh = heigh
	sprite.Completed = false

	surface, err := img.Load(path)
	if err != nil {
		return nil, fmt.Errorf("no surface %v", err)
	}

	sprite.texture, err = renderer.CreateTextureFromSurface(surface)
	if err != nil {
		return nil, fmt.Errorf("no texture %v", err)
	}
	surface.Free()

	return &sprite, nil
}

// Draw renders the sprite.
func (sprite *Sprite) Draw(renderer *sdl.Renderer) error {
	xSheet := (sprite.currentFrame % sprite.frameColums) * sprite.width

	var ySheet int
	if sprite.frameRows == 1 {
		ySheet = 0
	} else {
		ySheet = (sprite.currentFrame / sprite.frameRows) * sprite.heigh
	}
	//fmt.Printf("%d => %d, %d\n", sprite.currentFrame, xSheet, ySheet)

	renderer.Copy(sprite.texture, &sdl.Rect{X: int32(xSheet), Y: int32(ySheet), W: int32(sprite.width), H: int32(sprite.heigh)}, &sdl.Rect{X: int32(sprite.X), Y: int32(sprite.Y), W: int32(sprite.width), H: int32(sprite.heigh)})

	return nil
}

// Update updates the sprite.
func (sprite *Sprite) Update() {
	sprite.currentFrame++
	if sprite.currentFrame >= sprite.numFrames {
		sprite.Completed = true
	}
}

// Destroy frees all the sprite's resources.
func (sprite *Sprite) Destroy() {
	fmt.Println("Freeing sprite...")
	sprite.texture.Destroy()
}
